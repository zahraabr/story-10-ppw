from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
from .models import *

# Create your views here.
def index(request):
    return render(request, 'index.html')

def userLogin(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        user_dict = {'username':username, 'password':password}
        if user is not None:
            login(request, user)
            return redirect('/')
        
        else:
            message = {'message':'invalid username or password'}
            return render(request, 'login.html', message)
    
    else :
        return render(request, 'login.html')

def userLogout(request):
    request.session.flush()
    logout(request)
    return redirect('/')

def userSignup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profile.profilePhoto = form.cleaned_data.get('photo')
            user.save()
            return redirect('/login/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form' : form})
