from django.urls import path
from .views import *

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('login/', userLogin, name='login'),
    path('logout/', userLogout, name='logout'),
    path('signup/', userSignup, name='signup'),
]