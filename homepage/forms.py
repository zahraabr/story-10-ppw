from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=True, help_text='Required. Inform a valid name.')
    last_name = forms.CharField(max_length=30, required=True, help_text='Required. Inform a valid name.')
    email = forms.EmailField(max_length=254, required=True, help_text='Required. Inform a valid email address.')
    photo = forms.URLField(required=True, help_text='Required. Input a valid URL.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name','email', 'photo','password1', 'password2')
