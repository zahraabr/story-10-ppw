from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve, reverse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from django.conf import settings
from importlib import import_module
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
import time

# Create your tests here.
class Story10Test(TestCase):
    def test_URL_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_homepage_template_is_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class Story10FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        # super().setUp()
        # chrome_options = webdriver.ChromeOptions()
        # self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')
    
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Story10FunctionalTest, self).setUp()

    def tearDown(self) :
        self.driver.quit()
        super().tearDown()
    
    def test_user_register_then_login_then_logout(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        self.assertIn('hi! welcome', response_content)
        time.sleep(5)

        # Test when user signing up a new account
        self.driver.find_element_by_id('signUp').click()

        first = 'test'
        last = 'name'
        username = 'test'
        email = 'test@test.com'
        password = 'signup123'

        
        self.driver.find_element_by_id('id_username').send_keys(username)
        time.sleep(0.1)

        self.driver.find_element_by_id('id_first_name').send_keys(first)
        time.sleep(0.1)

        self.driver.find_element_by_id('id_last_name').send_keys(last)
        time.sleep(0.1)

        self.driver.find_element_by_id('id_email').send_keys(last)
        time.sleep(0.1)
        
        self.driver.find_element_by_id('id_photo').send_keys("https://picsum.photos/id/237/200/300")
        time.sleep(0.1)

        self.driver.find_element_by_id('id_password1').send_keys(password)
        time.sleep(0.1)

        
        self.driver.find_element_by_id('id_password2').send_keys(password)
        time.sleep(0.1)
        
        self.driver.find_element_by_id('signUp').click()

        time.sleep(5)

        # Log in
        self.driver.find_element_by_name('username').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('password').send_keys(password)
        time.sleep(3)
        self.driver.find_element_by_name('logIn').click()
        time.sleep(3)
        self.driver.find_element_by_id('logIn').click()
        time.sleep(3)


        response_page = self.driver.page_source
        self.assertIn('test', response_page)

        # Test when user wants to log out
        self.driver.find_element_by_id('logOut').click()
        time.sleep(3)

        response_content = self.driver.page_source
        self.assertIn('hi! welcome', response_content)

        self.driver.find_element_by_id('logIn').click()
        time.sleep(3)

        self.driver.find_element_by_name('username').send_keys(password)
        time.sleep(3)
        self.driver.find_element_by_name('password').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('logIn').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn("invalid username or password", response_page)


    def test_when_account_doesnt_exist_or_invalid_username_or_password(self):
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        self.assertIn('hi! welcome', response_content)
        time.sleep(5)

        self.driver.find_element_by_id('logIn').click()

        self.driver.find_element_by_id('username').send_keys("FunctionalTest")
        self.driver.find_element_by_id('password').send_keys("FunctionalTest")

        time.sleep(5)

        self.driver.find_element_by_id('logIn').click()

        response_content = self.driver.page_source
        self.assertIn("invalid username or password", response_content)
